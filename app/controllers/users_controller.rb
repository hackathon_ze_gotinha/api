class UsersController < ApplicationController
  before_action :authenticate, except: %i[login create]

  def login
    user = User.find_by(email: params['email'])
    if user.nil?
      render_fail('Perfil não encontrado')
    elsif user.try(:authenticate, params['password'])
      user.update_attributes(updated_at: Time.current,
                             token: user.generate_api_key)
      render json: { failed: 0, data: { token: user.token } }
    else
      render_fail('Senha inválida')
    end
  end

  def show
    render json: { failed: 0, data: @user.as_json(include: {vaccination: {include: :vaccine}}) }
  end

  def index
    # Nothing to do.
  end

  def update
    if @user.update_attributes(permit_update)
      render json: { failed: 0, data: @user.as_json }
    else
      render_fail(@user.errors)
    end
  end

  def create
    user = User.new(permit)
    if user.save
      render json: { failed: 0, data: user.as_json }
    else
      render_fail(user.errors)
    end
  end

    def add_vaccination
    vaccination = Vaccination.find_by(id: params[:id])
    if vaccination
      @user.vaccination << vaccination
      render json: { failed: 0 }
    else
      render_fail('Vacinação nao encontrada')
    end
  end

  private

  def permit
    params.permit(:email, :password, :password_confirmation, :name,
                  :birthdate, :is_pregnant, :cpf)
  end

  def permit_update
    params.permit(:password, :password_confirmation, :name,
                  :birthdate, :is_pregnant)
  end
end
