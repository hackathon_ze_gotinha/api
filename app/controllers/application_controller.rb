class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods

  def render_fail(msg, status = 200)
    render json: { failed: 1, error: msg }, status: status
  end

  def authenticate
    @user = nil
    authenticate_or_request_with_http_token do |token, _options|
      @user = User.get_user_by_token(token)
      valid_user?
    end
  end

  def admin_user?
    authenticate
    return if @user.nil?
    @user unless @user.level.zero?
  end

  private

  def valid_user?
    if @user.nil?
      render json: { failed: 1, error: 'Faça login' }, status: 401
    elsif ((@user.updated_at + 10.minutes) < Time.current) && @user.present?
      render json: { failed: 1, error: 'Login Expirado' }, status: 401
    else
      # @user.update_attributes(updated_at: Time.current)
      @user
    end
  end
end
