class VaccinationController < ApplicationController
  before_action :admin_user?, except: %i[show index]

  def create
    vaccination = Vaccination.new(permit)
    if vaccination.save
      render json: { failed: 0, data: vaccination.as_json }
    else
      render_fail(vaccination.errors)
    end
  end

  def create_vaccine
    if Vaccine.find_by(name: params[:name])
      render_fail('Vacina já cadastrada', 409)
    else
      vaccine = Vaccine.new(name: params[:name])
      if vaccine.save
        render json: { failed: 0, data: vaccine.as_json }
      else
        render_fail(vaccine.errors)
      end
    end
  end

  def update
    vaccination = Vaccination.find_by(id: params[:id])
    if vaccination.update_attributes(permit)
      render json: { failed: 0, data: vaccination.as_json }
    else
      render_fail(vaccination.errors)
    end
  end

  def destroy
    vaccination = Vaccination.find_by(id: params[:id])
    if vaccination
      vaccination.destroy
      render json: { failed: 0 }
    else
      render_fail('Vacinação não encontrada')
    end
  end

  def show
    vaccine = Vaccine.find_by(id: params[:id])
    render json: { failed: 0, data: vaccine.as_json(include: :vaccinations) }
  end

  def index
    vaccine = Vaccine.all
    render json: { failed: 0, data: vaccine.as_json(include: :vaccinations) }
  end

  private

  def permit
    params.permit(:target, :vaccine_id, :min_months, :max_months, :obs)
  end
end
