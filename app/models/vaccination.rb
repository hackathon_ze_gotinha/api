class Vaccination < ApplicationRecord
  has_and_belongs_to_many :user
  belongs_to :vaccine

  # Exception messages
  TARGET_CANT_BLANK = 'Preencha o público alvo'.freeze
  MIN_MONTHS_CANT_BLANK = 'Preencha o tempo mínimo de meses'.freeze
  MAX_MONTHS_CANT_BLANK = 'Preencha o tempo máximo de meses'.freeze
  OBS_CANT_BLANK = 'Preencha a obs com é reforço, dose única ou dose'.freeze

  # Validators
  validates :target, presence: { message: TARGET_CANT_BLANK }
  validates :min_months, presence: { message: MIN_MONTHS_CANT_BLANK }
  validates :max_months, presence: { message: MAX_MONTHS_CANT_BLANK }
  validates :obs, presence: { message: OBS_CANT_BLANK }
end
