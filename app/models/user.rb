class User < ApplicationRecord
  has_and_belongs_to_many :vaccination
  has_secure_password
  before_create do |doc|
    doc.token = doc.generate_api_key
  end

  # Exception messages
  NAME_CANT_BLANK = 'Preencha seu nome completo'.freeze
  CPF_CANT_BLANK = 'Preencha seu CPF'.freeze
  INVALID_CPF = 'Insira um CPF válido'.freeze
  CPF_EXISTS = 'CPF já existente'.freeze
  BIRTHDATE_CANT_BLANK = 'Preencha a sua data de nascimento'.freeze
  EMAIL_CANT_BLANK = 'Preencha seu email'.freeze
  EMAIL_EXISTS = 'Email já cadastrado'.freeze
  PASSWORD_CANT_BLANK = 'Preencha a sua senha'.freeze
  PASS_CONF_CANT_BLANK = 'Preencha a sua confirmação de senha'.freeze

  # Validators
  validates :name, presence: { message: NAME_CANT_BLANK }
  validates :cpf, presence: { message: CPF_CANT_BLANK }
  validates :cpf, cpf: { message: INVALID_CPF }
  validates :cpf, uniqueness: { message: CPF_EXISTS }
  validates :birthdate, presence: { message: BIRTHDATE_CANT_BLANK }
  validates :email, presence: { message: EMAIL_CANT_BLANK }
  validates :email, uniqueness: { message: EMAIL_EXISTS }
  validates :password, presence: { message: PASSWORD_CANT_BLANK }, on: :create
  validates :password_confirmation, presence: { message: PASS_CONF_CANT_BLANK },
                                    on: :create

  def generate_api_key
    require 'digest'
    loop do
      encrypt = name + password_digest + Time.now.to_i.to_s
      token = Digest::SHA512.hexdigest encrypt
      break token if User.find_by(token: token).nil?
    end
  end

  def self.get_user_by_token(token)
    User.find_by(token: token)
  end
end
