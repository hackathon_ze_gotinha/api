class Vaccine < ApplicationRecord
  has_many :vaccinations, dependent: :destroy

  # Exception messages
  NAME_CANT_BLANK = 'Preencha o nome da vacina'.freeze

  # Validators
  validates :name, presence: { message: NAME_CANT_BLANK }
end
