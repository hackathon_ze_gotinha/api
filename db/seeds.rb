# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(name: 'Vinicius Pinheiro', cpf: '42341155847', email: 'viny-pinheiro@hotmail.com', password: '123456789', password_confirmation: '123456789', birthdate: '1995-02-14', level: 1)
