class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :cpf
      t.string :email
      t.date :birthdate
      t.boolean :is_pregnant, default: false
      t.string :password_digest
      t.string :token

      t.timestamps
    end
  end
end
