class CreateVaccinations < ActiveRecord::Migration[5.1]
  def change
    create_table :vaccinations do |t|
      t.references :vaccine, foreign_key: true
      t.string :target
      t.string :min_months # if -1 is pregnant
      t.string :max_months # if -1 is pregnant
      t.string :obs

      t.timestamps
    end
  end
end
