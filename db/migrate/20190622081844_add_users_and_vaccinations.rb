class AddUsersAndVaccinations < ActiveRecord::Migration[5.1]
  def change
    create_table :users_vaccinations do |t|
      t.references :user, :vaccination
    end
  end
end
