# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190622081844) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "cpf"
    t.string "email"
    t.date "birthdate"
    t.boolean "is_pregnant", default: false
    t.string "password_digest"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "level", default: 0
  end

  create_table "users_vaccinations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "vaccination_id"
    t.index ["user_id"], name: "index_users_vaccinations_on_user_id"
    t.index ["vaccination_id"], name: "index_users_vaccinations_on_vaccination_id"
  end

  create_table "vaccinations", force: :cascade do |t|
    t.bigint "vaccine_id"
    t.string "target"
    t.string "min_months"
    t.string "max_months"
    t.string "obs"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["vaccine_id"], name: "index_vaccinations_on_vaccine_id"
  end

  create_table "vaccines", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "vaccinations", "vaccines"
end
