Rails.application.routes.draw do
  post 'users/login' => 'users#login'
  patch 'users/update' => 'users#update'
  post 'users/create' => 'users#create'
  get 'users/show' => 'users#show'
  post 'users/add_vaccination' => 'users#add_vaccination'

  post 'vaccination/create' => 'vaccination#create'
  post 'vaccination/create_vaccine' => 'vaccination#create_vaccine'
  patch 'vaccination/update/:id' => 'vaccination#update'
  delete 'vaccination/destroy/:id' => 'vaccination#destroy'
  get 'vaccination/show/:id' => 'vaccination#show'
  get 'vaccination/' => 'vaccination#index'


end
