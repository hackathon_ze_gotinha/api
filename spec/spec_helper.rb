require 'simplecov'
require "rspec/json_expectations"

SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new([
	  SimpleCov::Formatter::HTMLFormatter
])
SimpleCov.start
